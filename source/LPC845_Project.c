
#include <stdio.h>
#include <Registers.h>
#include <Leds.h>

int main(void) {
	//Initialize();
	SYSCON_SYSAHBCLKCTRL0_ADDR->GPIO1=1;

		GPIO_DIR1_ADDR->PIO1_0=1;
		GPIO_SET1_ADDR->PIO1_0=1;

		GPIO_DIR1_ADDR->PIO1_1=1;
		GPIO_SET1_ADDR->PIO1_1=1;

		GPIO_DIR1_ADDR->PIO1_2=1;
		GPIO_SET1_ADDR->PIO1_2=1;

	while(1) {
    	SET_LED(LED_GREEN);

    	SET_LED(LED_BLUE);

    	SET_LED(LED_RED);

    }
    return 0 ;
}
